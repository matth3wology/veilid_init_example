async fn send_message(veilid_api:veilid_core::VeilidAPI, message: Vec<T>, node_id: &str) {
    // Setup Target and Message to send over RPC
    let target = veilid_api
        .parse_as_target(node_id)
        .await
        .unwrap();

    // Send Message to Target over RPC
    veilid_api
        .routing_context()
        .app_message(target, message)
        .await
        .unwrap();
}
