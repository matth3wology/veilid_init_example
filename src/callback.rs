use std::sync::Arc;

// This is the Veilid Callback function that is used by the Veilid API.
pub fn get_veilid_callback(sender: flume::Sender<veilid_core::VeilidUpdate>) -> veilid_core::UpdateCallback {
    return Arc::new(move |change: veilid_core::VeilidUpdate| {
        if let Err(e) = sender.send(change) {
            let change = e.into_inner();
            println!("error sending veilid update callback: {:?}", change);
        }
    });
}
