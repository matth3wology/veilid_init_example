use veilid_core;
mod callback;
mod config;

#[tokio::main]
async fn main() {
    println!("Starting node..");
    // Create a Sender and Receiver channel. This is used to send and receive messages
    // from Veilid.
    let (sender, receiver): (
        flume::Sender<veilid_core::VeilidUpdate>,
        flume::Receiver<veilid_core::VeilidUpdate>,
    ) = flume::unbounded();

    // These are the callback and configuration structs used to instantiate a new API
    let cb = callback::get_veilid_callback(sender);
    let veilid_config = config::get_veilid_config();

    // Setup the Veilid API object
    let vapi = veilid_core::api_startup(cb, veilid_config).await.unwrap();

    // Attach the API to the Veilid network. This should use bootstrap.veilid.net to start getting peers.
    println!("Attaching node..");
    vapi.attach().await.unwrap();

    // Print out this users Node ID
    let node_id = vapi
        .get_state()
        .await
        .unwrap()
        .config
        .config
        .network
        .routing_table
        .node_id[0];
    println!("Node-ID: {}:{}", node_id.kind, node_id.value);

    // Receive message Handler
    let recv_handler = tokio::spawn(async move {
        {
            loop {
                // Print a VeildidUpdater that is sent from the Veilid API callback
                // Need to handle the different messages that are received. Also need to
                // add some graceful way of shutting down the loop.
                let res = receiver.recv_async().await.unwrap();

                match res {
                    veilid_core::VeilidUpdate::AppMessage(_) => {
                        println!("Received an AppMessage");
                    }

                    veilid_core::VeilidUpdate::AppCall(_) => {
                        println!("Received an AppCall");
                    }

                    veilid_core::VeilidUpdate::Attachment(_) => {
                        println!("Received an Attachment");
                    }

                    veilid_core::VeilidUpdate::Config(_) => {
                        println!("Received an Config");
                    }

                    veilid_core::VeilidUpdate::RouteChange(_) => {
                        println!("Received RouteChange");
                    }

                    veilid_core::VeilidUpdate::ValueChange(_) => {
                        println!("Received ValueChange");
                    }

                    veilid_core::VeilidUpdate::Network(_) => {
                        println!("Received network data.");
                    }

                    veilid_core::VeilidUpdate::Log(_) => {
                        println!("Received Log data.");
                    }

                    veilid_core::VeilidUpdate::Shutdown => {
                        println!("Received a shutdown!");
                        break;
                    }
                }
            }
        }
    });

    recv_handler.await.unwrap();

    // Need to shutdown the Veilid API
}
